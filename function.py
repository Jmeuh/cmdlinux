import psycopg2
import psycopg2.extensions
import logging

conn = psycopg2.connect(dbname="cmdlinux", user="postgres")
cur = conn.cursor()


def register():

    print("Répertorier une nouvelle commande :")
    name = input("Nom : ")
    description = input("description : ")

    sql = f" INSERT INTO command (name, description) VALUES ('{name}', $${description}$$);"

    cur.execute(sql)

    choice = input("Valider l'enregistrement (O/n)")
    if choice == "o" or choice == "O" or choice == "":
        conn.commit()
    elif choice == "n" or choice == "N":
        conn.close()
    else:
        register()


def show():

    sql = """ SELECT * FROM command ORDER BY name ASC; """
    cur.execute(sql)
    result = (cur.fetchall())
    for i in result:
        print(f"{i[0]}| {i[1]} : {i[2]}")


def idControl():

    sql = "SELECT MAX(id) FROM command;"
    cur.execute(sql)
    lastId = cur.fetchall()[0][0]
    sql = f"ALTER SEQUENCE command_id_seq restart with {lastId+1}"
    cur.execute(sql)


def delete():

    id = input("entrer l'id à supprimer > ")
    sql = f"DELETE FROM command WHERE id = '{id}'"
    cur.execute(sql)
    conn.commit()


def search():

    sql = "SELECT name, description FROM command;"
    cur.execute(sql)
    descriptions = cur.fetchall()

    print("Seul les mots d'au moins 4 caractères sont pris en compte")
    search = input("recherche : ")
    result = False

    lockedWord = ["le", "la", "les", "de", "du", "des", "pour", "sur", "dans"]

    for d in descriptions:
        description = d[1]

        for i in description.split():
            for j in search.split():
                for lw in lockedWord:
                    if j == i and j != lw:
                        result = f'{d[0]} : {d[1]}'
                        print(f"\n- {result}")
                    elif j == 'q':
                        break
                    else:
                        break
